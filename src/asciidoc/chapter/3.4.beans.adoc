[[beans-factory-scopes]]
=== Bean scopes
When you create a bean definition, you create a __recipe__ for creating actual instances
of the class defined by that bean definition. The idea that a bean definition is a
recipe is important, because it means that, as with a class, you can create many object
instances from a single recipe.

You can control not only the various dependencies and configuration values that are to
be plugged into an object that is created from a particular bean definition, but also
the __scope__ of the objects created from a particular bean definition. This approach is
powerful and flexible in that you can __choose__ the scope of the objects you create
through configuration instead of having to bake in the scope of an object at the Java
class level. Beans can be defined to be deployed in one of a number of scopes: out of
the box, the Spring Framework supports five scopes, three of which are available only if
you use a web-aware `ApplicationContext`.

The following scopes are supported out of the box. You can also create
<<beans-factory-scopes-custom,a custom scope.>>

[[beans-factory-scopes-tbl]]
.Bean scopes
|===
| Scope| Description

| <<beans-factory-scopes-singleton,singleton>>
| (Default) Scopes a single bean definition to a single object instance per Spring IoC
  container.

| <<beans-factory-scopes-prototype,prototype>>
| Scopes a single bean definition to any number of object instances.

| <<beans-factory-scopes-request,request>>
| Scopes a single bean definition to the lifecycle of a single HTTP request; that is,
  each HTTP request has its own instance of a bean created off the back of a single bean
  definition. Only valid in the context of a web-aware Spring `ApplicationContext`.

| <<beans-factory-scopes-session,session>>
| Scopes a single bean definition to the lifecycle of an HTTP `Session`. Only valid in
  the context of a web-aware Spring `ApplicationContext`.

| <<beans-factory-scopes-global-session,global session>>
| Scopes a single bean definition to the lifecycle of a global HTTP `Session`. Typically
  only valid when used in a portlet context. Only valid in the context of a web-aware
  Spring `ApplicationContext`.

| <<beans-factory-scopes-application,application>>
| Scopes a single bean definition to the lifecycle of a `ServletContext`. Only valid in
  the context of a web-aware Spring `ApplicationContext`.
|===

[NOTE]
====
As of Spring 3.0, a __thread scope__ is available, but is not registered by default. For
more information, see the documentation for
{javadoc-baseurl}/org/springframework/context/support/SimpleThreadScope.html[`SimpleThreadScope`].
For instructions on how to register this or any other custom scope, see
<<beans-factory-scopes-custom-using>>.
====



[[beans-factory-scopes-singleton]]
==== The singleton scope
Only one __shared__ instance of a singleton bean is managed, and all requests for beans
with an id or ids matching that bean definition result in that one specific bean
instance being returned by the Spring container.

To put it another way, when you define a bean definition and it is scoped as a
singleton, the Spring IoC container creates __exactly one__ instance of the object
defined by that bean definition. This single instance is stored in a cache of such
singleton beans, and __all subsequent requests and references__ for that named bean
return the cached object.

image::images/singleton.png[width=400]

Spring's concept of a singleton bean differs from the Singleton pattern as defined in
the Gang of Four (GoF) patterns book. The GoF Singleton hard-codes the scope of an
object such that one __and only one__ instance of a particular class is created __per
ClassLoader__. The scope of the Spring singleton is best described as __per container
and per bean__. This means that if you define one bean for a particular class in a
single Spring container, then the Spring container creates one __and only one__ instance
of the class defined by that bean definition. __The singleton scope is the default scope
in Spring__. To define a bean as a singleton in XML, you would write, for example:

[source,xml,indent=0]
[subs="verbatim,quotes"]
----
	<bean id="accountService" class="com.foo.DefaultAccountService"/>

	<!-- the following is equivalent, though redundant (singleton scope is the default) -->
	<bean id="accountService" class="com.foo.DefaultAccountService" scope="singleton"/>
----



