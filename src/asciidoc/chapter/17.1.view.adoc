[[view-jsp-formtaglib-inputtag]]
===== input标签

This tag renders an HTML 'input' tag using the bound value and type='text' by default.
For an example of this tag, see <<view-jsp-formtaglib-formtag>>. Starting with Spring
3.1 you can use other types such HTML5-specific types like 'email', 'tel', 'date', and
others.


[[view-jsp-formtaglib-checkboxtag]]
===== The checkbox tag

This tag renders an HTML 'input' tag with type 'checkbox'.

Let's assume our `User` has preferences such as newsletter subscription and a list of
hobbies. Below is an example of the `Preferences` class:

[source,java,indent=0]
[subs="verbatim,quotes"]
----
	public class Preferences {

		private boolean receiveNewsletter;
		private String[] interests;
		private String favouriteWord;

		public boolean isReceiveNewsletter() {
			return receiveNewsletter;
		}

		public void setReceiveNewsletter(boolean receiveNewsletter) {
			this.receiveNewsletter = receiveNewsletter;
		}

		public String[] getInterests() {
			return interests;
		}

		public void setInterests(String[] interests) {
			this.interests = interests;
		}

		public String getFavouriteWord() {
			return favouriteWord;
		}

		public void setFavouriteWord(String favouriteWord) {
			this.favouriteWord = favouriteWord;
		}
	}
----

The `form.jsp` would look like:

[source,xml,indent=0]
[subs="verbatim,quotes"]
----
	<form:form>
		<table>
			<tr>
				<td>Subscribe to newsletter?:</td>
				<%-- Approach 1: Property is of type java.lang.Boolean --%>
				<td><form:checkbox path="preferences.receiveNewsletter"/></td>
			</tr>

			<tr>
				<td>Interests:</td>
				<%-- Approach 2: Property is of an array or of type java.util.Collection --%>
				<td>
					Quidditch: <form:checkbox path="preferences.interests" value="Quidditch"/>
					Herbology: <form:checkbox path="preferences.interests" value="Herbology"/>
					Defence Against the Dark Arts: <form:checkbox path="preferences.interests" value="Defence Against the Dark Arts"/>
				</td>
			</tr>

			<tr>
				<td>Favourite Word:</td>
				<%-- Approach 3: Property is of type java.lang.Object --%>
				<td>
					Magic: <form:checkbox path="preferences.favouriteWord" value="Magic"/>
				</td>
			</tr>
		</table>
	</form:form>
----

There are 3 approaches to the `checkbox` tag which should meet all your checkbox needs.

* Approach One - When the bound value is of type `java.lang.Boolean`, the
  `input(checkbox)` is marked as 'checked' if the bound value is `true`. The `value`
  attribute corresponds to the resolved value of the `setValue(Object)` value property.
* Approach Two - When the bound value is of type `array` or `java.util.Collection`, the
  `input(checkbox)` is marked as 'checked' if the configured `setValue(Object)` value is
  present in the bound `Collection`.
* Approach Three - For any other bound value type, the `input(checkbox)` is marked as
  'checked' if the configured `setValue(Object)` is equal to the bound value.

Note that regardless of the approach, the same HTML structure is generated. Below is an
HTML snippet of some checkboxes:

[source,xml,indent=0]
[subs="verbatim,quotes"]
----
	<tr>
		<td>Interests:</td>
		<td>
			Quidditch: <input name="preferences.interests" type="checkbox" value="Quidditch"/>
			<input type="hidden" value="1" name="_preferences.interests"/>
			Herbology: <input name="preferences.interests" type="checkbox" value="Herbology"/>
			<input type="hidden" value="1" name="_preferences.interests"/>
			Defence Against the Dark Arts: <input name="preferences.interests" type="checkbox" value="Defence Against the Dark Arts"/>
			<input type="hidden" value="1" name="_preferences.interests"/>
		</td>
	</tr>
----

What you might not expect to see is the additional hidden field after each checkbox.
When a checkbox in an HTML page is __not__ checked, its value will not be sent to the
server as part of the HTTP request parameters once the form is submitted, so we need a
workaround for this quirk in HTML in order for Spring form data binding to work. The
`checkbox` tag follows the existing Spring convention of including a hidden parameter
prefixed by an underscore ("_") for each checkbox. By doing this, you are effectively
telling Spring that "__the checkbox was visible in the form and I want my object to
which the form data will be bound to reflect the state of the checkbox no matter what__".


